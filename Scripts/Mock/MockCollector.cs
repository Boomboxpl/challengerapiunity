using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class MockCollector : MonoBehaviour
{
    public string scoreName;
    public string username;
    public string password;
    ChallengerScoreSender scoreSender;

    [Button]
    public void SendScore()
    {
        if(scoreSender == null)
        {
            scoreSender = FindObjectOfType<ChallengerScoreSender>();
        }
        if (!scoreSender.IsAuthenticated)
        {
            LoginDTO login = new LoginDTO() { username = username, password = password };
            scoreSender.Login(login, result => SendScoreInternal(), error => Debug.Log(error));
        }
        else
        {
            SendScoreInternal();
        }
    }

    void SendScoreInternal()
    {
        HighScoreDTO dto = new HighScoreDTO();
        dto.name = scoreName;
        dto.subscore_set = new List<SubScoreDTO>();
        foreach (var mock in GetComponentsInChildren<MockComponent>())
        {
            dto.subscore_set.Add(mock.GetSubscore());
        }
        scoreSender.SendHighscore(dto, response => Debug.Log("Succes " + response), error => Debug.Log(error));
    }
}
