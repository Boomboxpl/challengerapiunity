using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MockValue : MockComponent
{
    [SerializeField] SingleValue value;

    protected override SubScoreValue GetSubscoreValue()
    {
        return value;
    }
}
