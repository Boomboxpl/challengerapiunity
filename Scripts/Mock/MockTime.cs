using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class MockTime : MockComponent
{
    public float valueMultiplier = 1;
    public float secondsStep = 1;
    public float step = 1;
    public AnimationCurve mockingTime;

    protected override SubScoreValue GetSubscoreValue()
    {
        var resultList = new List<TimeScoreValue.TimeScores>();
        float maxTime = mockingTime.keys.Last().time;
        DateTime startDate = DateTime.Now.AddSeconds(-maxTime / step * secondsStep);
        float time = 0;
        while(time < maxTime + step)
        {
            resultList.Add(new TimeScoreValue.TimeScores() 
            { 
                time = startDate.AddSeconds(time / step * secondsStep), 
                value = mockingTime.Evaluate(time) * valueMultiplier
            });
            time += step;
        }
        TimeScoreValue result = new TimeScoreValue();
        result.timeScores = resultList;
        return result;
    }
}
