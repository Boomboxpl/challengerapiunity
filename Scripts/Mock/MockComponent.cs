using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MockComponent : MonoBehaviour
{
    [SerializeField] int priority;
    [SerializeField] string subscoreName;

    public SubScoreDTO GetSubscore()
    {
        SubScoreDTO subscore = new SubScoreDTO();
        subscore.priority = priority;
        subscore.name = subscoreName;
        subscore.values = GetSubscoreValue();
        return subscore;
    }

    protected abstract SubScoreValue GetSubscoreValue();
}
