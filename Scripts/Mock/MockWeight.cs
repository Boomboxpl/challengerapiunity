using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MockWeight : MockComponent
{
    public List<WeightString> weightScores;

    protected override SubScoreValue GetSubscoreValue()
    {
        List<WeightScoreValue.WeightScores> resultList = new List<WeightScoreValue.WeightScores>();
        foreach(var weightScore in weightScores)
        {
            resultList.Add(new WeightScoreValue.WeightScores() { weight = weightScore.weight, value = weightScore.value });
        }
        return new WeightScoreValue() { weightScores = resultList };
    }

    [Serializable]
    public class WeightString
    {
        public float weight;
        public string value;
    }
}
