using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.IO;
using NaughtyAttributes;

public class ChallengerScoreSender : MonoBehaviour
{
    const string refreshTokenKey = "REFRESH_TOKEN";

    const long unauthorizedCode = 401;

    public bool verbose = false;

    [SerializeField] string defaultUrl = "https://challenger-backend.fly.dev/api/";
    [SerializeField] string authUrl = "authorization/token/";
    [SerializeField] string refreshUrl = "authorization/token/refresh/";
    [SerializeField] string postUrl = "highscores/create/";
    [SerializeField] string secretKey = "";
    [SerializeField] int gameId;
    [HideInInspector] [SerializeField] string accessToken = "";
    [HideInInspector] [SerializeField] string refreshToken = "";

    public bool IsAuthenticated { get; private set; }

    void Start()
    {
        Refresh();
    }

    [Button]
    public void Logout()
    {
        accessToken = "";
        refreshToken = "";
        PlayerPrefs.DeleteKey(refreshTokenKey);
    }

    public void Login(LoginDTO dto, Action<string> onSucess = null, Action<string> onFailed = null)
    {
        StartCoroutine(SendingRoutine(defaultUrl + authUrl,
            SerializeObject(dto),
            response => { IsAuthenticated = true; onSucess?.Invoke(response); },
            error => { IsAuthenticated = false; onFailed?.Invoke(error); }));
    }

    public void Refresh(Action<string> onSucess = null, Action<string> onFailed = null)
    {
        if (string.IsNullOrEmpty(refreshToken))
        {
            refreshToken = PlayerPrefs.GetString(refreshTokenKey);
            if (string.IsNullOrEmpty(refreshToken))
            {
                IsAuthenticated = false;
                return;
            }
        }
        StartCoroutine(SendingRoutine(defaultUrl + refreshUrl,
            SerializeObject(new RefreshDTO() { refresh = refreshToken}),
            response => { IsAuthenticated = true; onSucess?.Invoke(response); },
            error => { IsAuthenticated = false; onFailed?.Invoke(error); }));
    }

    public void SendHighscore(HighScoreDTO dto, Action<HighScoreDTO> onSucess = null, Action<string> onFailed = null)
    {
        dto.game = gameId;
        StartCoroutine(SendingRoutine(defaultUrl + postUrl,
            SerializeObject(dto), 
            result => onSucess?.Invoke(DeserializeObject<HighScoreDTO>(result)), 
            onFailed));
    }

    public static string SerializeObject<T>(T obj)
    {
        JsonSerializer serializer = new JsonSerializer();
        serializer.NullValueHandling = NullValueHandling.Ignore;
        serializer.TypeNameHandling = TypeNameHandling.Auto;
        serializer.Formatting = Formatting.None;
        string result;
        using (StringWriter strWriter = new StringWriter())
        {
            using (JsonWriter writer = new JsonTextWriter(strWriter))
            {
                serializer.Serialize(writer, obj, typeof(T));
                result = strWriter.ToString();
            }
        }
        return result;
    }

    public static T DeserializeObject<T>(string obj)
    {
        var settings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.Auto,
            NullValueHandling = NullValueHandling.Ignore,
        };
        return JsonConvert.DeserializeObject<T>(obj, settings);
    }

    IEnumerator SendingRoutine(string url, string body, Action<string> onSucess, Action<string> onFailed)
    {
        if (verbose)
        {
            Debug.Log(body);
        }
        using (UnityWebRequest www = UnityWebRequest.Post(url, body))
        {
            var uploadHandler = www.uploadHandler;
            byte[] bodyToSend = new System.Text.UTF8Encoding().GetBytes(body);
            www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyToSend);
            www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            www.SetRequestHeader("Content-Type", "application/json");
            www.SetRequestHeader("GAME-API", secretKey);

            www.SetRequestHeader("Authorization", "Bearer " + accessToken);
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                if(www.responseCode == unauthorizedCode && !url.Contains(refreshUrl))
                {
                    Refresh(res => StartCoroutine(SendingRoutine(url, body, onSucess, onFailed)), onFailed);
                    yield break;
                }
                onFailed?.Invoke(www.downloadHandler.text);
            }
            else
            {
                string response = www.downloadHandler.text;
                TrySaveRefresh(response);
                onSucess?.Invoke(response);
            }
        }
    }

    void TrySaveRefresh(string response)
    {
        LoginResponseDTO loginResponse = DeserializeObject<LoginResponseDTO>(response);
        if (!string.IsNullOrEmpty(loginResponse.access))
        {
            accessToken = loginResponse.access;
        }
        if (!string.IsNullOrEmpty(loginResponse.refresh)){
            refreshToken = loginResponse.refresh;
        }
        PlayerPrefs.SetString(refreshTokenKey, refreshToken);
    }
}
