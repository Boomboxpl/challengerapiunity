using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

[Serializable]
public class SingleValue : SubScoreValue
{
    public float value;
}
