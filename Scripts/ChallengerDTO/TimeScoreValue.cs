using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

[Serializable]
public class TimeScoreValue : SubScoreValue
{
    public List<TimeScores> timeScores;

    [Serializable]
    public class TimeScores
    {
        public DateTime time;
        public object value;
    }
}
