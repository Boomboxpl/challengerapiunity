using System.Collections.Generic;
using System;
using Newtonsoft.Json;

[Serializable]
public struct HighScoreDTO
{
    public int id;
    public string name;
    public int game;
    public List<SubScoreDTO> subscore_set;

    public override string ToString()
    {
        return ChallengerScoreSender.SerializeObject(this);
    }
}

[Serializable]
public struct SubScoreDTO
{
    public int priority;
    public string name;
    public SubScoreValue values;

    public override string ToString()
    {
        return ChallengerScoreSender.SerializeObject(this);
    }
}

[Serializable]
public struct LoginDTO
{
    public string username;
    public string password;
}

[Serializable]
public struct LoginResponseDTO
{
    public string access;
    public string refresh;

    public override string ToString()
    {
        return ChallengerScoreSender.SerializeObject(this);
    }
}

[Serializable]
public struct RefreshDTO
{
    public string refresh;

    public override string ToString()
    {
        return ChallengerScoreSender.SerializeObject(this);
    }
}


