using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System;

[Serializable]
public class WeightScoreValue : SubScoreValue
{
    public List<WeightScores> weightScores;

    [Serializable]
    public class WeightScores
    {
        public float weight;
        public object value;
    }
}
